﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SpawnCtrl : MonoBehaviour {

	public List<GameObject> vehiclesPrefabs = new List<GameObject>(); 

	private List<Spawn> _spawnNodes = new List<Spawn>(); 
	private GameObject _vehiclesRoot;

	private System.Diagnostics.Stopwatch _spawnTimer = new System.Diagnostics.Stopwatch();
	private System.Diagnostics.Stopwatch _spawnRateTimer = new System.Diagnostics.Stopwatch();

	private int _deltaSpawn = 0;
	private float _spawnRate = 0;

	private void Awake() {

		this._vehiclesRoot = GameObject.Find("Vehicles");

		GameObject[] objs = GameObject.FindGameObjectsWithTag("Spawner");
		foreach(GameObject g in objs) {
			this._spawnNodes.Add(g.GetComponent<Spawn>());
		}

		this._spawnTimer.Start();
		this._spawnRateTimer.Start();

		StartCoroutine(this._Spawn());
	}

	void Update() {

		double time = this._spawnRateTimer.Elapsed.TotalSeconds;
		if (time > 2) {

			this._spawnRate = (float) (this._deltaSpawn / time);
			this._deltaSpawn = 0;

			this._spawnRateTimer.Restart();

		}
	}

	private IEnumerator _Spawn() {

		List<int> indexes = new List<int>();
		List<int> default_indexes = Enumerable.Range(0, this._spawnNodes.Count).ToList<int>();

		while (true) {

			if ((this._spawnTimer.Elapsed.TotalSeconds > Globals.SPAWN_RATE) && (this._GetNumberOfSpawnedObj() < Globals.SPAWN_MAX)) {


				Spawn spawn_node;
				int random_index;
				int spawn_index;
				indexes.Clear();
				indexes.AddRange(default_indexes);

				while(true) {

					// Pick a random_index number from the available ones
					random_index = Mathf.RoundToInt(Random.value * (indexes.Count - 1));
					spawn_index = indexes[random_index];
					spawn_node = this._spawnNodes[spawn_index];

					// Remove the random_index as an available value
					indexes.RemoveAt(random_index);

					if (spawn_node.isValid) {
						// Job's done
						break;
					} else if(indexes.Count == 0) {
					// All spawners are invalid
						indexes.AddRange(default_indexes);
						yield return null;
					}

				}

				// Spawn the obj at a random SpawnNode
				int obj_index = Mathf.RoundToInt(Random.value * (this.vehiclesPrefabs.Count - 1));

				GameObject new_obj = Instantiate(this.vehiclesPrefabs[obj_index]);

				new_obj.SetActive(true);
				new_obj.transform.parent = this._vehiclesRoot.transform;
				new_obj.transform.GetComponent<CarCtrl>().nextNode = spawn_node.GetComponent<LaneNode>();

				this._deltaSpawn++;

				this._spawnTimer.Restart();
			}

			yield return null;
		}

	}

	private int _GetNumberOfSpawnedObj() {
		return this._vehiclesRoot.transform.childCount;
	}

	void OnGUI() {
		int w = Screen.width, h = Screen.height;

		GUIStyle style = new GUIStyle();
		style.alignment = TextAnchor.UpperLeft;
		style.fontSize = h * 2 / 100;
		style.normal.textColor = new Color(0.0f, 0.0f, 0.5f, 1.0f);


		string text = string.Format("Vehicles : {0:0}", this._GetNumberOfSpawnedObj());
		Rect rect = new Rect(0, 10, w, h * 2 / 100);
		GUI.Label(rect, text, style);

		text = string.Format("Spawn rate : {0:n2} car/sec", this._spawnRate);
		rect = new Rect(0, 20, w, h * 2 / 100);
		GUI.Label(rect, text, style);

	}

}
