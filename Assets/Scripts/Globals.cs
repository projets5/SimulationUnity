﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

class Globals {
	//public const int CAMERA_SIM_WIDTH = 640;
	//public const int CAMERA_SIM_HEIGHT = 480;
	public const int CAMERA_SIM_WIDTH = 1280;
	public const int CAMERA_SIM_HEIGHT = 720;
	public const int CAMERA_JPEG_QUALITY = 85;

	public const float SPAWN_RATE = 0.1f; // 1 every x sec
	public const int SPAWN_MAX = 500; // Maximum number of obj

	//public const int SPAWN_MAX = 1; // Maximum number of obj
	//public const float SPAWN_RATE = 0.25f; // 1 every x sec
	//public const int SPAWN_MAX = 200; // Maximum number of obj

}
