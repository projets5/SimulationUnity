﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Xml.Linq;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Jobs;
using UnityEngine;
using UnityEngine.Rendering;

public class DatasetGenerator : MonoBehaviour {


	// Object
	public GameObject rotating_obj;
	public string annotation_name="car";
	public string dataset_name;

	public int obj_angle_start = 90;
	public int obj_angle_end = 270;
	public int obj_angle_step_per_it = 10;

	public int camera_distance_start = 6;
	public int camera_distance_end = 10;
	public int camera_distance_step_per_it = 1;

	public int camera_height_start = 2;
	public int camera_height_end = 8;
	public int camera_height_step_per_it = 2;

	// Consts
	private const int _width = Globals.CAMERA_SIM_WIDTH;
	private const int _height = Globals.CAMERA_SIM_HEIGHT;
	private const int _byte_array_length = _width * _height * 3;
	private const string _dst_folder = "Datasets\\";
	private const int _jpeg_quality = Globals.CAMERA_JPEG_QUALITY;

	// Others
	private string _folder_path;
	private Camera _camera;
	private AsyncGPUReadbackRequest _gpu_request;
	private NativeArray<byte> _texture_data;
	private int _nb_of_frame_to_capture;
	private int _compressed_image_length;

	// Use this for initialization
	void Start () {
		// RenderTexture setup
		RenderTexture rt = new RenderTexture(_width, _height, 16) {
			enableRandomWrite = true
		};
		rt.Create();

		// Camera setup
		this._camera = this.GetComponent<Camera>();
		//this._camera.enabled = false;
		this._camera.targetTexture = rt;

		// NativeArray setup
		this._texture_data = new NativeArray<byte>(_byte_array_length, Allocator.Persistent);

		// Merge meshes
		this._MergeObjMeshes();

		// Output folder path
		this._folder_path = _dst_folder + dataset_name + "\\";

		// Clear the images output folder
		if (Directory.Exists(this._folder_path)) {
			Directory.Delete(this._folder_path, true);
		}

		// Create the dataset output folder
		if (!Directory.Exists(this._folder_path)) {
			Directory.CreateDirectory(this._folder_path);
		}

		this.StartCoroutine(_CaptureCoroutine());
	}

	private void OnApplicationQuit() {
		this._texture_data.Dispose();
	}

	private void _MergeObjMeshes() {
		// The rotating_obj has multiple meshes so we need to merge them to find the bounding box

		MeshFilter[] meshFilters = this.rotating_obj.GetComponentsInChildren<MeshFilter>();
		CombineInstance[] combine = new CombineInstance[meshFilters.Length];

		int i = 0;
		while (i < meshFilters.Length) {
			combine[i].mesh = meshFilters[i].sharedMesh;
			combine[i].transform = meshFilters[i].transform.localToWorldMatrix;
			meshFilters[i].gameObject.SetActive(false);
			i++;
		}

		this.rotating_obj.transform.GetComponent<MeshFilter>().mesh = new Mesh();
		this.rotating_obj.transform.GetComponent<MeshFilter>().mesh.CombineMeshes(combine);
		this.rotating_obj.transform.gameObject.SetActive(true);
	}

	private IEnumerator _CaptureCoroutine() {
		int nb_of_frame_to_capture = (int) Mathf.Floor((this.obj_angle_end - this.obj_angle_start) / this.obj_angle_step_per_it);

		for (
			int camera_height = this.camera_height_start;
			camera_height <= this.camera_height_end;
			camera_height += this.camera_height_step_per_it
		) {
			for (
				int camera_distance = this.camera_distance_start;
				camera_distance <= this.camera_distance_end;
				camera_distance += this.camera_distance_step_per_it
			) {

				// Set camera position
				this.transform.eulerAngles = new Vector3(Mathf.Rad2Deg * Mathf.Atan(camera_height / (float) camera_distance), 180, 0);
				this.transform.position = new Vector3(this.transform.position.x, camera_height, camera_distance);

				for (
					int obj_angle = this.obj_angle_start;
					obj_angle <= this.obj_angle_end;
					obj_angle += this.obj_angle_step_per_it
				) {

					// Set obj position
					Vector3 rot = this.rotating_obj.transform.eulerAngles;
					this.rotating_obj.transform.eulerAngles = new Vector3(rot.x, 180 - obj_angle, rot.z);
	

					// Render all camera
					this._RenderCamera();

					// Process current requests
					yield return this._ProcessRequest();

					// Compression
					this._CompressImage();

					// Generate bounding box annotation
					this._SaveBoundingRectAsXml(camera_height, camera_distance, obj_angle);

					// Save image to disk
					this._WriteImagesToDisk(camera_height, camera_distance, obj_angle);
				}
			}
		}

		Debug.Log("DONE : " + this.dataset_name);
	}

	private void _RenderCamera() {
		// Render
		this._camera.Render();

		// Enqueue async gpu request for the texture gpu -> cpu transfert
		this._gpu_request = AsyncGPUReadback.Request(this._camera.targetTexture, 0, TextureFormat.RGB24);
	}

	private IEnumerator _ProcessRequest() {

		// Wait for request to be ready
		while(!this._gpu_request.done) {
			yield return null;
		}

		// Retrive data from the request
		this._texture_data.CopyFrom(this._gpu_request.GetData<byte>());
	}

	[DllImport("JpegEncodingWorkerDLL")]
	private static extern unsafe int _CompressImageInPlace(
		void* src,
		int width,
		int height,
		int jpeg_quality
	);
	private void _CompressImage() {

		this._compressed_image_length = 0;

		unsafe {
			// Use a native plugin to optimise jpeg encoding
			this._compressed_image_length = _CompressImageInPlace(
				this._texture_data.GetUnsafePtr(),
				_width,
				_height,
				_jpeg_quality
			);
		}

	}

	private bool _bounding_box_valid = false;
	private void _SaveBoundingRectAsXml(int camera_height, int camera_distance, int obj_angle) {
		string file_name = camera_height + "_" + camera_distance + "_" + obj_angle;
		Rect r = this._getObjBoundingBox();

		XDocument doc = new XDocument(
			new XElement("annotation",
				new XElement("folder", this.dataset_name),
				new XElement("filename", file_name + ".jpg"),
				new XElement("path", "./" + file_name + ".jpg"),
				new XElement("source",
					new XElement("database", "Unknown")
				),
				new XElement("size",
					new XElement("width", _width),
					new XElement("height", _height),
					new XElement("depth", 3)
				),
				new XElement("segmented", 0),
				new XElement("object",
					new XElement("name", this.annotation_name),
					new XElement("pose", "Unspecified"),
					new XElement("truncated", 0),
					new XElement("difficult", 0),
					new XElement("bndbox",
						new XElement("xmin", (int) r.xMin),
						new XElement("ymin", (int) r.yMin),
						new XElement("xmax", (int) r.xMax),
						new XElement("ymax", (int) r.yMax)
					)
				)
			)
		);

		doc.Save(this._folder_path + file_name + ".xml");
	}

	private Rect _getObjBoundingBox() {
		Vector3[] verts = this.rotating_obj.GetComponent<MeshFilter>().mesh.vertices;

		for (int v = 0; v < verts.Length; v++) {
			verts[v] = this.rotating_obj.transform.TransformPoint(verts[v]);
			verts[v] = this._camera.WorldToScreenPoint(verts[v]);
			verts[v].y = _height - verts[v].y;
		}

		Vector3 min = verts[0];
		Vector3 max = verts[0];

		for (int v = 0; v < verts.Length; v++) {
			min = Vector3.Min(min, verts[v]);
			max = Vector3.Max(max, verts[v]);
		}

		return Rect.MinMaxRect(min.x, min.y, max.x, max.y);
	}

	private void _WriteImagesToDisk(int camera_height, int camera_distance, int obj_angle) {

		if (this._compressed_image_length == 0) {
			Debug.LogError("Error in jpeg native plugin : " + obj_angle);
		} else {
			File.WriteAllBytes(
				this._folder_path + camera_height + "_" + camera_distance + "_" + obj_angle + ".jpg",
				this._texture_data.Slice(0, this._compressed_image_length).ToArray()
			);
		}

	}

}
