﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DummyCube : MonoBehaviour {

	private float _y;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		this.transform.Translate(new Vector3(0, 2*Mathf.Cos(Time.fixedTime), 0) * Time.deltaTime);
	}
}
