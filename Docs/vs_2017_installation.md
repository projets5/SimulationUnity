# Visual studio community 2017
## Installation sur Windows
Page de téléchargement [Visual Studio Community 2017](https://www.visualstudio.com/fr/).  

![Visual Studio Community 2017 download page](./Imgs/vs2017_download_page.JPG)

Il faut installer les composantes C# et C++.
