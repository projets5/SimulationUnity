#include <direct.h>
#include <string>
#include <iostream>
#include <fstream>
#include <Windows.h>
#include <chrono>
#include <turbojpeg.h>

#include "IUnityInterface.h"
#include "DebugCPP.h"

using namespace std::chrono;

// Get current working dir (same as SolutionDir)
#define GetCurrentDir _getcwd
std::string GetCurrentWorkingDir(void) {
	char buff[FILENAME_MAX];
	GetCurrentDir(buff, FILENAME_MAX);
	std::string current_working_dir(buff);
	return current_working_dir;
}

const std::string current_working_dir = GetCurrentWorkingDir();

extern "C" void UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API _EncodeByteArrayToJpeg(
	const unsigned char src[],
	int width,
	int height,
	char file_path[],
	int jpeg_quality
) {
	const int len = width * height * 3;
	const std::string absolute_jpeg_file_path = current_working_dir + "\\" + std::string(file_path);

	unsigned char *compressed_image = NULL;
	long unsigned int jpeg_size = 0;

	// Top to bottom conversion
	unsigned char *buffer = new unsigned char[len];
	for (int i = 0; i < height; i++) {
		memcpy(buffer + (i * width * 3), src +  ((height - 1 - i) * width * 3), width * 3);
	}

	// Code found at https://stackoverflow.com/questions/9094691/examples-or-tutorials-of-using-libjpeg-turbos-turbojpeg
	tjhandle jpeg_compressor = tjInitCompress();

	tjCompress2(
		jpeg_compressor,
		buffer,
		width,
		0,
		height,
		TJPF_RGB,
		&compressed_image,
		&jpeg_size,
		TJSAMP_444,
		jpeg_quality,
		TJFLAG_FASTDCT
	);


	// Write data to file
	std::ofstream file(absolute_jpeg_file_path, std::ofstream::binary);

	file.write((char *) compressed_image, jpeg_size);
	file.flush();

	file.close();

	// Free memory
	tjDestroy(jpeg_compressor);
	tjFree(compressed_image);

	delete[] buffer;
}

extern "C" int UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API _CompressImageInPlace(
	unsigned char src[],
	int width,
	int height,
	int jpeg_quality
) {
	const long unsigned int len = width * height * 3;

	unsigned char *compressed_image = NULL;
	long unsigned int jpeg_size = 0;

	// Top to bottom conversion
	unsigned char *buffer = new unsigned char[len];
	for (int i = 0; i < height; i++) {
		memcpy(buffer + (i * width * 3), src +  ((height - 1 - i) * width * 3), width * 3);
	}

	// Code found at https://stackoverflow.com/questions/9094691/examples-or-tutorials-of-using-libjpeg-turbos-turbojpeg
	tjhandle jpeg_compressor = tjInitCompress();

	tjCompress2(
		jpeg_compressor,
		buffer,
		width,
		0,
		height,
		TJPF_RGB,
		&compressed_image,
		&jpeg_size,
		TJSAMP_444,
		jpeg_quality,
		TJFLAG_FASTDCT
	);

	// Should never be the case, but what if we have more byte after compression ?
	if (len >= jpeg_size) {
		memcpy(src, compressed_image, jpeg_size);
	} else {
		return 0;
	}

	// Free memory
	tjDestroy(jpeg_compressor);
	tjFree(compressed_image);

	delete[] buffer;

	return jpeg_size;
}

