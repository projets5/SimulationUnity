﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class AlignOnGameGrid : MonoBehaviour {

	private const float _tileSize = 2.5f;
	private const float _halfTileSize = 2.5f;

	void Update () {

		//Vector3 pos = this.transform.position - new Vector3(_halfTileSize, 0, _halfTileSize);
		Vector3 pos = this.transform.position;
		pos = new Vector3(
			Mathf.Round(pos.x / _tileSize) * _tileSize,
			0,
			Mathf.Round(pos.z / _tileSize) * _tileSize
		);
		//pos += new Vector3(_halfTileSize, 0, _halfTileSize);

		this.transform.position = pos;

	}
}
