﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn : MonoBehaviour {

	public bool isValid {
		get;
		private set;
	} = true;

	private void OnTriggerEnter(Collider other) {
		isValid = false;
	}

	private void OnTriggerExit(Collider other) {
		isValid = true;
	}

}
