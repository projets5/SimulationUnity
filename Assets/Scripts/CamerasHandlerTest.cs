﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using Unity.Collections;
using Unity.Jobs;
//using System.Drawing;
//using System.Drawing.Imaging;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Rendering;

public class CamerasHandlerTest : MonoBehaviour {

	// Params
	public bool upload_or_write = true; // true=upload, false=write, FPS output drops drastically if true
	public bool logFps = true;

	// Consts
	private const int _width = Globals.CAMERA_SIM_WIDTH;
	private const int _height = Globals.CAMERA_SIM_HEIGHT;
	private const int _byte_array_length = _width * _height * 3;
	private const string _dst_folder = "SimOutput\\";
	private Rect _screen_rect = new Rect(0, 0, _width, _height);


	// List
	private List<Camera> _cameras = new List<Camera>();
	private List<AsyncGPUReadbackRequest> _async_requests = new List<AsyncGPUReadbackRequest>();

	// Chorno
	private System.Diagnostics.Stopwatch _computation_allowed_time_chrono = new System.Diagnostics.Stopwatch();
	private System.Diagnostics.Stopwatch _computation_full_time_chrono = new System.Diagnostics.Stopwatch();

	// Soft cap the time allowed to compute
	private const int _computation_time = 15;

	// Counter used for frame name generation (TODO remove?)
	private int _camera_index = 0;
	private int _frame_counter = 0;

	// Jobs
	List<JobHandle> _jobs;
	List<NativeArray<byte>> _job_src_arrays;
	List<NativeArray<int>> _compressed_images_length;

	private void Awake() {
		// Turn off v-sync (Editor)
		QualitySettings.vSyncCount = 0;

		// Fps limit the main camera
		Application.targetFrameRate = 30;
	}

	private void Start() {

		// Create camera list
		this._cameras = new List<Camera>(gameObject.GetComponentsInChildren<Camera>());

		// Clear the images output folder
		if (Directory.Exists(_dst_folder)) {
			Directory.Delete(_dst_folder, true);
		}
		// Create the simulation output folder
		if (!Directory.Exists(_dst_folder)) {
			Directory.CreateDirectory(_dst_folder);
		}

		// Cameras setup
		for (int i=0; i<this._cameras.Count; i++) {
			Camera c = this._cameras[i];

			RenderTexture rt = new RenderTexture(_width, _height, 16) {
				enableRandomWrite = true
			};
			rt.Create();

			// Cam setup
			c.enabled = false;
			c.targetTexture = rt;

			// Create the cameras' images output folder
			Directory.CreateDirectory(_dst_folder + this._GetCameraName(c) + "/");
			//Directory.CreateDirectory(_dst_folder + c.name + "/");
		}

		// Jobs setup
		//this._jobs = new List<JobHandle>(this._cameras.Count);
		//this._job_src_arrays = new List<NativeArray<byte>>(this._cameras.Count);
		//this._compressed_images_length = new List<NativeArray<int>>(this._cameras.Count);

		//for (int c = 0; c < this._cameras.Count; c++) {
		//	this._jobs.Add(new JobHandle());
		//	this._job_src_arrays.Add(new NativeArray<byte>(_byte_array_length, Allocator.Persistent));
		//	this._compressed_images_length.Add(new NativeArray<int>(1, Allocator.Persistent));
		//}

		// Start the coroutine
		this.StartCoroutine(this._CaptureCoroutine());

		//Debug.LogError("START");
	}

	private void OnApplicationQuit() {

		//for (int c = 0; c < this._cameras.Count; c++) {
		//	this._jobs[c].Complete();
		//	this._job_src_arrays[c].Dispose();
		//	this._compressed_images_length[c].Dispose();
		//}

	}

	private IEnumerator _CaptureCoroutine() {

		int nb_group = 4;
		int group_size = this._cameras.Count / nb_group;

		while (true) {

			// Restart the full computation time chrono
			this._computation_full_time_chrono.Restart();
			int a = 0;
			for(int i=0; i< nb_group; i++) {
				// Render all camera
				this._RenderCameras(i * group_size, group_size);

				// Create AsyncGPUReadbackRequests
				this._CreateAsyncGPUReadbackRequests(i * group_size, group_size);

				//Debug.Log("GROUP " + i);
				yield return this._ProcessDoneRequest();
			}

			// TODO Pipeline this space ?, since _WaitAsyncGPUReadbackRequests can take
			// multiple frame to complete we could fill this gap

			// Wait for requests to complete
			yield return this._WaitAsyncGPUReadbackRequests();

			// Wait for jobs to complete
			//yield return this._WaitForJobsToComplete();

			//if(upload_or_write) {
			//	// Upload images
			//	_UploadImagesToBackend();
			//} else {
			//	// Write to disk
			//	yield return _WriteImagesToDisk();
			//}

			// Computation frame rate
			if (this.logFps) {
				Debug.LogError(1000f / this._computation_full_time_chrono.ElapsedMilliseconds);
			}

			// Did the whole list of camera
			if (this._camera_index == 0) {
				this._frame_counter++;
			}

			// Skip 1 frame
			yield return null;
		}

	}

	private void _RenderCameras(int offset, int size) {
		for (int i=offset; i < offset + size; i++) {
			Camera c = this._cameras[i];

			c.Render();
		}
	}

	private void _CreateAsyncGPUReadbackRequests(int offset, int size) {
		for (int i = offset; i < offset + size; i++) {
			Camera c = this._cameras[i];

			// Enqueue async gpu request for the texture gpu -> cpu transfert
			this._async_requests.Add(AsyncGPUReadback.Request(c.targetTexture, 0, TextureFormat.RGB24));
		}
	}

	private IEnumerator _ProcessDoneRequest() {
		// Each available request must be process in the same frame.
		// Async response have a life time of 1 frame and are ready at the start of it, else we wait on the next frame

		List<AsyncGPUReadbackRequest> reqs = this._async_requests.FindAll((r) => r.done == true);
		foreach (AsyncGPUReadbackRequest req in reqs) {
			if (req.hasError) {
				Debug.LogError("AsyncRequest Error");
			} else {
				// Create a job (Encode and export image)
				//this._ScheduleJob(req);
				//NativeArray<byte> a = req.GetData<byte>();
				//Debug.Log(this._camera_index + " " + req.GetData<byte>().Length + " -- " + _byte_array_length);
			}

			this._async_requests.Remove(req);
		}

		// Next
		//this._incrCameraIndex();

		// Give it 1 more frame
		yield return null;
		this._computation_allowed_time_chrono.Restart();
	}

	private IEnumerator _WaitAsyncGPUReadbackRequests() {
		while (this._async_requests.Count > 0) {
			yield return this._ProcessDoneRequest();
		}
	}

	private void _incrCameraIndex() {
		this._camera_index = (this._camera_index + 1) % this._cameras.Count;
	}

	//private void _ScheduleJob(AsyncGPUReadbackRequest req) {
	//	// Get active job_arrays
	//	NativeArray<byte> job_src_array = this._job_src_arrays[this._camera_index];

	//	// Retrive data from the request
	//	job_src_array.CopyFrom(req.GetData<byte>());

	//	// Setup new job
	//	//JpegEncodingJob job = new JpegEncodingJob {
	//	//	src = job_src_array,
	//	//	compressed_image_length = this._compressed_images_length[this._camera_index],
	//	//	frame_counter = this._frame_counter,
	//	//	camera_index = this._camera_index,
	//	//};

	//	// Add job to the jobs queue
	//	this._jobs[this._camera_index] = job.Schedule();
	//}

	private IEnumerator _WaitForJobsToComplete() {

		// Wait for jobs to complete
		for (int j = 0; j < this._jobs.Count; j++) {

			// If job is not completed, give it 1 more frame
			while (!this._jobs[j].IsCompleted) {
				yield return null;
				this._computation_allowed_time_chrono.Restart();
			}

			// Make sure we are done with it
			this._jobs[j].Complete();
		}

	}

	private void _UploadImagesToBackend() {

		WWWForm form = new WWWForm();

		for (int c = 0; c < this._cameras.Count; c++) {

			if (this._compressed_images_length[c][0] == 0) {
				Debug.LogError("Error in jpeg native plugin : " + c);
			} else {
				int compressed_image_length = this._compressed_images_length[c][0];
				string filename;
				filename = this._GetCameraName(c) + "_" + 0; // TODO add timestamps

				form.AddBinaryData(
					"media",
					this._job_src_arrays[c].Slice(0, compressed_image_length).ToArray(), // TODO GC alloc every time
					 filename + ".jpg",
					"image/jpeg"
				);
			}

		}

		// Send the form
		UnityWebRequest handshake;
		handshake = UnityWebRequest.Post("http://192.168.0.102:8080/upload", form); // Jimme's home laptop
		//handshake = UnityWebRequest.Post("http://192.168.0.76:8080/upload", form); // Jimmy's home server
		handshake.SendWebRequest();

		// We don't care about the response...
	}

	private IEnumerator _WriteImagesToDisk() {
		// FPS output drops drastically

		for (int c = 0; c < this._cameras.Count; c++) {

			if(this._computation_allowed_time_chrono.ElapsedMilliseconds > _computation_time) {
				yield return null;
				this._computation_allowed_time_chrono.Restart();
			}

			if (this._compressed_images_length[c][0] == 0) {
				Debug.LogError("Error in jpeg native plugin : " + c);
			} else {
				int compressed_image_length = this._compressed_images_length[c][0];

				File.WriteAllBytes(
					_dst_folder + this._GetCameraName(c) + "\\" + this._frame_counter + ".jpg",
					this._job_src_arrays[c].Slice(0, compressed_image_length).ToArray()
				);
			}

		}
	}

	private string _GetCameraName(Camera c) {
		//	return (c.transform.parent.parent.name[14] + "" + c.transform.name[8]).PadLeft(3);
		//return c.transform.parent.parent.name + "_" + c.transform.name;
		return c.transform.name;
	}
	private string _GetCameraName(int c) {
		return this._GetCameraName(this._cameras[c]);
	}
}