﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrafficLightCtrl : MonoBehaviour {

	public List<GameObject> lights = new List<GameObject>();
	private Dictionary<string, int> _lightIndexByName = new Dictionary<string, int>();

	public enum TrafficLightState {
		Red,
		Green,
		Yellow
	}
	private TrafficLightState _currentState = TrafficLightState.Red;

	private float _timeLeft;
	private int _lightIndex;

	private const float _greenLightTime = 2;
	private const float _yellowLightTime = 1;
	//private const float _greenLightTime = 5;
	//private const float _yellowLightTime = 3;

	private void Start() {
		// Setup for the first light
		_lightIndex = 1;
		_currentState = TrafficLightState.Red;

		int i = 0;
		foreach(GameObject g in this.lights) {
			this._lightIndexByName.Add(g.transform.name, i++);
		}

	}

	void Update () {
		_UpdateState();
	}

	private void _UpdateState() {
		this._currentState = this._GetNextState();
	}

	private TrafficLightState _GetNextState() {
		this._timeLeft -= Time.deltaTime;
		bool is_time_over = this._timeLeft <= 0;

		switch (this._currentState) {
			case TrafficLightState.Green: {

				if(is_time_over) {
					this._timeLeft = _yellowLightTime;
					this.lights[this._lightIndex].GetComponent<BoxCollider>().enabled = true;

					return TrafficLightState.Yellow;
				} else {
					return TrafficLightState.Green;
				}

			}
			case TrafficLightState.Yellow: {

				if (is_time_over) {
					return TrafficLightState.Red;
				} else {
					return TrafficLightState.Yellow;
				}

			}
			case TrafficLightState.Red:
			default: {
				this._timeLeft = _greenLightTime;
				this._lightIndex = (this._lightIndex + 1) % 4;

				this.lights[this._lightIndex].GetComponent<BoxCollider>().enabled = false;

				return TrafficLightState.Green;
			}
		}
	}

	public TrafficLightState GetStateForLight(string name) {
		if(this._lightIndexByName[name] == this._lightIndex) {
			return this._currentState;
		} else {
			return TrafficLightState.Red;
		}
	}

	private void OnDrawGizmos() {

		// Draw a circle hover the light to see the state
		for (int l = 0; l < this.lights.Count; l++) {

			Gizmos.color = (l == this._lightIndex) ? this._GetStateColor(this._currentState) : Color.red;
			Gizmos.DrawSphere(
				this.lights[l].transform.position + Vector3.up * 5f,
				0.3f
			);

		}
	}

	private Color _GetStateColor(TrafficLightState state) {
		switch (state) {
			case TrafficLightState.Green: {
				return Color.green;
			}
			case TrafficLightState.Yellow: {
				return Color.yellow;
			}
			case TrafficLightState.Red:
			default: {
				return Color.red;
			}
		}
	}

}

