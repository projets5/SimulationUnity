﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntersectionColliderCtrl : MonoBehaviour {

	private void OnTriggerEnter(Collider other) {
		other.GetComponent<CarCtrl>().SetIsInIntersection(true);

	}

	private void OnTriggerExit(Collider other) {
		other.GetComponent<CarCtrl>().SetIsInIntersection(false);
	}


}
