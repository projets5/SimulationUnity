﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[ExecuteInEditMode]
public class RoadSegmentInEditor : MonoBehaviour {

	public bool regenerateRoad;

	public enum RoadDirection {
		North,
		East,
		South,
		West
	}
	public RoadDirection roadDirection = RoadDirection.North;

	[Range(1, 512)]
	public int length = 5;
	public float tileSize = 5;

	public enum LaneDirection {
		F_Foward,
		F_Right,
		F_Left,
		F_FowardRight,
		F_FowardLeft,
		B_Foward,
		B_Right,
		B_Left,
		B_FowardRight,
		B_FowardLeft,
	}
	public List<LaneDirection> lanes = new List<LaneDirection>();
	private List<List<GameObject>> _lanesTiles = new List<List<GameObject>>();


	private GameObject _simpleRoadTile;
	private GameObject _fowardArrowRoadTile;
	private GameObject _rightArrowRoadTile;
	private GameObject _doubleWhiteLineRoadTile;
	private GameObject _yellowLineRoadTile;

	private void Awake() {
		
		this._LoadPrefabs();

		this._GenerateRoad();

	}

	void Update () {
		// Road can't be move while runing
		if (Application.isPlaying) return;

		// Align on a grid
		Vector3 tmp_v = this.transform.position;
		this.transform.position = new Vector3(
			Mathf.Round(tmp_v.x / this.tileSize) * this.tileSize,
			0,
			Mathf.Round(tmp_v.z / this.tileSize) * this.tileSize
		);

		if(this.regenerateRoad) {
			this._GenerateRoad();

			this.regenerateRoad = false;
		}
	}

	private void _ClearTiles() {
		// Delete children
		while(this.transform.childCount > 0) {
			Transform obj = this.transform.GetChild(0);

			obj.parent = null;

			if (Application.isPlaying) {
				Destroy(obj.gameObject);
			} else {
				DestroyImmediate(obj.gameObject);
			}
		}

		// Clear the list
		foreach (List<GameObject> lane in this._lanesTiles) {
			lane.Clear();
		}
		this._lanesTiles.Clear();
	}

	private void _GenerateRoad() {
		this._ClearTiles();

		// Create Empty GO for lanes
		for (int l = 0; l < this.lanes.Count; l++) {
			GameObject empty = new GameObject();

			empty.transform.name = "lane_" + l;
			empty.transform.position = this.transform.position;

			empty.transform.SetParent(this.transform);

			this._lanesTiles.Add(new List<GameObject>());
		}


		// Populate lanes with tiles
		for (int l = 0; l < this._lanesTiles.Count; l++) {
			for (int t = 0; t <= this.length; t++) {

				GameObject new_tile;


				switch (this.lanes[l]) {
					case LaneDirection.B_FowardLeft:
					case LaneDirection.B_FowardRight:
					case LaneDirection.B_Left:
					case LaneDirection.B_Right:
					case LaneDirection.B_Foward: {
						
						if (t == 0) { // First tile
							new_tile = Instantiate(this._fowardArrowRoadTile) as GameObject;
						} else {
							new_tile = Instantiate(this._yellowLineRoadTile) as GameObject;
						}

						new_tile.transform.Rotate(0, 180, 0);
						break;
					}
					case LaneDirection.F_FowardLeft:
					case LaneDirection.F_FowardRight:
					case LaneDirection.F_Left:
					case LaneDirection.F_Right:
					case LaneDirection.F_Foward: {

						if (t == this.length) { // Last tile
							new_tile = Instantiate(this._fowardArrowRoadTile) as GameObject;
						} else {
							new_tile = Instantiate(this._yellowLineRoadTile) as GameObject;
						}
						
						break;
					}
					default: {
						new_tile = Instantiate(this._simpleRoadTile) as GameObject;
						break;
					}
				}

				new_tile.transform.position = this.GetTilePos(t, l);
				new_tile.transform.name = "tile_" + t;
				new_tile.transform.SetParent(this.transform.GetChild(l).transform);

				this._lanesTiles[l].Add(new_tile);
			}
		}
	}

	private void OnDrawGizmos() {
		Vector3 start_pos = this.transform.position;
		Vector3 end_pos = this.GetEndPos();

		Gizmos.color = Color.green;
		Gizmos.DrawSphere(start_pos, 0.3f);

		Gizmos.color = Color.blue;
		Gizmos.DrawSphere(end_pos, 0.3f);

		Gizmos.color = Color.yellow;
		Gizmos.DrawLine(start_pos, end_pos);
	}

	public Vector3 GetEndPos() {
		Vector3 start_pos = this.transform.position;
		Vector3 end_pos = start_pos;

		switch (this.roadDirection) {
			case RoadDirection.North: {
				end_pos += new Vector3(0, 0, (this.length + 1) * this.tileSize);
				break;
			}
			case RoadDirection.East: {
				end_pos += new Vector3((this.length + 1) * this.tileSize, 0, 0);
				break;
			}
			case RoadDirection.South: {
				end_pos += new Vector3(0, 0, -(this.length + 1) * this.tileSize);
				break;
			}
			case RoadDirection.West: {
				end_pos += new Vector3(-(this.length + 1) * this.tileSize, 0, 0);
				break;
			}
		}

		return end_pos;
	}

	public Vector3 GetTilePos(int index, int offset_index) {
		Vector3 start_pos = this.transform.position;
		Vector3 tile_pos = start_pos;

		switch (this.lanes[offset_index]) {
			case LaneDirection.B_FowardLeft:
			case LaneDirection.B_FowardRight:
			case LaneDirection.B_Left:
			case LaneDirection.B_Right:
			case LaneDirection.B_Foward: {
				offset_index -= this.lanes.Count / 2;

				index--;
				break;
			}
			case LaneDirection.F_FowardLeft:
			case LaneDirection.F_FowardRight:
			case LaneDirection.F_Left:
			case LaneDirection.F_Right:
			case LaneDirection.F_Foward:
			default: {
				offset_index -= this.lanes.Count / 2 - 1;
				break;
			}
		}


		switch (this.roadDirection) {
			case RoadDirection.North: {
				tile_pos +=
					new Vector3(0, 0, index * this.tileSize) +
					new Vector3(offset_index * this.tileSize, 0, 0)
				;
				break;
			}
			case RoadDirection.East: {
				tile_pos += new Vector3(index * this.tileSize, 0, 0) +
					new Vector3(0, 0, offset_index * this.tileSize)
				;
				break;
			}
			case RoadDirection.South: {
				tile_pos += new Vector3(0, 0, -index * this.tileSize) +
					new Vector3(-offset_index * this.tileSize, 0, 0)
				;
				break;
			}
			case RoadDirection.West: {
				tile_pos += new Vector3(-index * this.tileSize, 0, 0);
				break;
			}
		}

		return tile_pos;
	}



	private void _LoadPrefabs() {
		this._simpleRoadTile = Resources.Load(
			"PolygonCity/Prefabs/Environments/SM_Env_Road_Bare_01",
			typeof(GameObject)
		) as GameObject;
		this._fowardArrowRoadTile = Resources.Load(
			"PolygonCity/Prefabs/Environments/SM_Env_Road_Arrow_01",
			typeof(GameObject)
		) as GameObject;
		this._rightArrowRoadTile = Resources.Load(
			"PolygonCity/Prefabs/Environments/SM_Env_Road_Arrow_02",
			typeof(GameObject)
		) as GameObject;
		this._doubleWhiteLineRoadTile = Resources.Load(
			"PolygonCity/Prefabs/Environments/SM_Env_Road_Lines_01",
			typeof(GameObject)
		) as GameObject;
		this._yellowLineRoadTile = Resources.Load(
			"PolygonCity/Prefabs/Environments/SM_Env_Road_YellowLines_01",
			typeof(GameObject)
		) as GameObject;
	}

}
