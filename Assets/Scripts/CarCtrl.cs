﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static TrafficLightCtrl;

public class CarCtrl : MonoBehaviour {

	public LaneNode lastNode;
	public LaneNode nextNode;

	public float safetyDistFromColider = 3;

	private float _speed = 0;
	private float _acceleration = 10;
	private float _brakePower = 10;

	private float _startTime;
	private float _totalDist;

	private const float _topSpeed = 20; // TODO Random offset ?
	private const float _rayLength = 30;

	public bool _isInIntersection = false;

	private float _distToCollider = -1;
	private float _oldDistToCollider = -1;

	private Ray _collisionRay = new Ray();
	private RaycastHit _ray_cast_hit = new RaycastHit();

	void Start () {

		// Setup the car on its nextNode
		this._UpdateNextNode();

	}
	
	void Update () {

		if(!this._isInIntersection) { // Disable raycast in intersections

			// Detect if there is something in front
			this._collisionRay.origin = this.transform.position + Vector3.up;
			this._collisionRay.direction = this.transform.TransformDirection(Vector3.forward);
			if (Application.isEditor) {
				Debug.DrawRay(
					this.transform.position + Vector3.up,
					this.transform.TransformDirection(Vector3.forward) * this.safetyDistFromColider
				//this.transform.TransformDirection(Vector3.forward) * _rayLength
				);
			}
			Physics.Raycast(
				this._collisionRay,
				out this._ray_cast_hit,
				_rayLength
			);
			if (this._ray_cast_hit.transform != null) {

				this._oldDistToCollider = this._distToCollider;
				this._distToCollider = this._ray_cast_hit.distance;

				if (this._oldDistToCollider < 0) { // Can't 
					this._oldDistToCollider = 0;
				}

				this._speed = this._CalculateSpeedWhileColliding();


			} else {
				this._speed = this._CalculateSpeed();
			}

		} else {
			this._speed = this._CalculateSpeed();
		}


		// Path update
		if (this.nextNode) {

			float dist = this._speed * Time.deltaTime;
			float dist_to_next_node = Vector3.Distance(this.transform.position, this.nextNode.transform.position);
			if (dist >= dist_to_next_node) {
				// We are further than the next_node
				
				// Set the car at the next_node
				this._UpdateNextNode();

				// Remove the distance since we are now at the old next_node 
				dist -= dist_to_next_node;
			}

			this.transform.Translate(Vector3.forward * dist);
		}
	}

	private void _UpdateNextNode() {

		if(this.nextNode) {
			this.lastNode = this.nextNode;
			this.nextNode = this.nextNode.GetRandomChildNode();

			if (this.nextNode) {

				this._startTime = Time.time;
				this._totalDist = Vector3.Distance(this.lastNode.transform.position, this.nextNode.transform.position);

				Vector3 diff_v = this.nextNode.transform.position - this.lastNode.transform.position;
				this.transform.rotation = Quaternion.Euler(0, Mathf.Rad2Deg * Mathf.Atan2(diff_v.x, diff_v.z), 0);
				this.transform.position = this.lastNode.transform.position;

			}
		}
	}

	private void OnDrawGizmos() {

		Gizmos.DrawRay(
			this.transform.position + Vector3.up,
			this.transform.TransformDirection(Vector3.forward) * this.safetyDistFromColider
		);

	}

	private float _GetAngle() {
		Vector3 rot = this.transform.rotation * Vector3.forward;
		return Mathf.Rad2Deg * Mathf.Atan2(rot.x, rot.z);
	}

	private float _CalculateSpeed() {
		return Mathf.Min(this._speed + this._acceleration * Time.deltaTime, _topSpeed);
	}

	private float _CalculateSpeedWhileColliding() {

		// Get the relative speed with the collider
		float relative_speed = (this._distToCollider - this._oldDistToCollider) / Time.deltaTime;
		float safe_dist_to_collider = this._distToCollider - this.safetyDistFromColider;

		//Debug.Log(relative_speed + " -- " + safe_dist_to_collider);

		if(safe_dist_to_collider < 0) {
		// The car is too close to the collider
			return 0;
		} else if (relative_speed < 0) {
		// The car need to slow down base on it's distance and relative speed with the collider
			return Mathf.Max(this._speed - (Mathf.Abs(relative_speed)) / safe_dist_to_collider * 0.1f, 0); // TODO Find a real way
		} else {
		// The car is slower or as fast as the collider
			if (this._distToCollider < this.safetyDistFromColider) {
				return this._speed; // (keep a safe distance) TODO test ?
			} else {
				return this._CalculateSpeed();// (try to catch up)
			}

		}

	}

	public void SetIsInIntersection(bool val) {
		this._isInIntersection = val;
	}

}
