﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaneNode : MonoBehaviour {
	public Color nodeColor = Color.green;
	
	public List<GameObject> parentNodes = new List<GameObject>();
	private List<GameObject> _childrenNodes = new List<GameObject>();
	
	void Awake () {

		foreach (GameObject node in this.parentNodes) {
			node.GetComponent<LaneNode>()._childrenNodes.Add(this.gameObject);
		}

	}

	void Update () {

	}

	private void OnDrawGizmos() {
		Vector3 pos = this.transform.position;

		Gizmos.color = nodeColor;
		Gizmos.DrawSphere(pos, 0.3f);
		
		foreach(GameObject node in this.parentNodes) {
			Gizmos.DrawLine(pos, node.transform.position);
		}
	}

	public LaneNode GetRandomChildNode() {
		int count = this._childrenNodes.Count;
		if(count > 0) {
			return this._childrenNodes[Mathf.FloorToInt(Random.value * count)].GetComponent<LaneNode>();
		} else {
			return null;
		}
	}
}

