# Unity
## Installation sur Windows
Page de téléchargement [Unity beta 2018.1.x 64bit](https://unity3d.com/fr/unity/beta-download).  
La meilleur option est d'utiliser Unity Hub qui centralise les différentes version s'il en a une nouvelle. 

![unity hub download](./Imgs/unity_hub_download_page.JPG)
![unity hub](./Imgs/unity_hub_beta.JPG)
![unity hub](./Imgs/unity_hub_install.JPG)

Lors de l'installation, cocher uniquement les cases ci haut.


