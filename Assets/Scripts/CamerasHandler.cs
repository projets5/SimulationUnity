﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
//using System.Drawing;
//using System.Drawing.Imaging;
using UnityEngine;
using UnityEngine.Rendering;
using Unity.Collections;
using Unity.Jobs;
using Unity.Collections.LowLevel.Unsafe;
using UnityEngine.Networking;
using System.Text;
using System;
using System.Xml.Linq;
using System.Collections.Concurrent;
using System.Threading;

public class CamerasHandler : MonoBehaviour {

	// Params
	public bool upload_or_write = true; // false=write, true=upload, FPS output drops drastically if true
	public bool exportXmlRectangles = false;
	public bool logFps = true;

	// Consts
	private const int _width = Globals.CAMERA_SIM_WIDTH;
	private const int _height = Globals.CAMERA_SIM_HEIGHT;
	private const int _byte_array_length = _width * _height * 3;
	private const string _dst_folder = "SimOutput\\";
	private const int _jpeg_quality = Globals.CAMERA_JPEG_QUALITY;

	// Async Queue
	Queue<AsyncGPUReadbackRequest> _requests = new Queue<AsyncGPUReadbackRequest>();

	// List
	private List<Camera> _cameras = new List<Camera>();

	// Chorno
	private System.Diagnostics.Stopwatch _computation_allowed_time_chrono = new System.Diagnostics.Stopwatch();
	private System.Diagnostics.Stopwatch _computation_full_time_chrono = new System.Diagnostics.Stopwatch();

	// Soft cap the time allowed to compute
	private const int _computation_time = 15;

	// Counter used for frame name generation (TODO remove?)
	private int _camera_index = 0;
	private int _frame_counter = 0;

	// Jobs
	List<JobHandle> _jobs;
	List<NativeArray<byte>> _job_src_arrays;
	List<NativeArray<int>> _compressed_images_length;

	// Bounds detections (xml)
	private GameObject _vehicles;
	private List<Plane[]> _planes = new List<Plane[]>();
	private Collider[] _vehiclesColliders;
	private List<GameObject> _vehiclesRendered = new List<GameObject>();
	private ConcurrentQueue<XmlJobData> _xmlQueue = new ConcurrentQueue<XmlJobData>();
	private List<Thread> _xmlThreads = new List<Thread>();
	private int _numberOfXmlThread = 4;


	private void Awake() {
		// Turn off v-sync (Editor)
		QualitySettings.vSyncCount = 0;

		// Fps limit the main camera
		Application.targetFrameRate = 30;
	
	}

	private void Start() {

		// Create camera list
		List<GameObject> camera_gos = new List<GameObject>(GameObject.FindGameObjectsWithTag("InterCam"));
		foreach(GameObject c in camera_gos) {
			this._cameras.Add(c.GetComponent<Camera>());
		}

		// Get vehicles reference
		this._vehicles = GameObject.Find("Vehicles");
		if(this.exportXmlRectangles) {
			for(int t=0; t<this._numberOfXmlThread; t++) {
				Thread th = new Thread(() => {
					while (true) {

						XmlJobData data;

						if(this._xmlQueue.TryDequeue(out data)) {
							data.doc.Save(data.path);
						}

					}

				});

				th.Start();

				this._xmlThreads.Add(th);
			}
		}

		// Clear the images output folder
		if (Directory.Exists(_dst_folder)) {
			Directory.Delete(_dst_folder, true);
		}
		// Create the simulation output folder
		if (!Directory.Exists(_dst_folder)) {
			Directory.CreateDirectory(_dst_folder);
		}

		// Cameras setup
		for(int i=0; i<this._cameras.Count; i++) {
			Camera c = this._cameras[i];

			RenderTexture rt = new RenderTexture(_width, _height, 16) {
				enableRandomWrite = true
			};
			rt.Create();

			// Cam setup
			c.enabled = false;
			c.targetTexture = rt;

			// Create the cameras' images output folder
			Directory.CreateDirectory(_dst_folder + this._GetCameraName(c) + "/");
			//Directory.CreateDirectory(_dst_folder + c.name + "/");

			if(this.exportXmlRectangles) {
				Directory.CreateDirectory(_dst_folder + "Datasets\\" + i + "\\");

				// Get cameras planes
				this._planes.Add(GeometryUtility.CalculateFrustumPlanes(c));
			}
		}

		// Jobs setup
		this._jobs = new List<JobHandle>(this._cameras.Count);
		this._job_src_arrays = new List<NativeArray<byte>>(this._cameras.Count);
		this._compressed_images_length = new List<NativeArray<int>>(this._cameras.Count);

		for (int c = 0; c < this._cameras.Count; c++) {
			this._jobs.Add(new JobHandle());
			this._job_src_arrays.Add(new NativeArray<byte>(_byte_array_length, Allocator.Persistent));
			this._compressed_images_length.Add(new NativeArray<int>(1, Allocator.Persistent));
		}

		// Start the coroutine
		this.StartCoroutine(this._CaptureCoroutine());

		//Debug.LogError("START");
	}

	private void OnApplicationQuit() {

		for (int c = 0; c < this._cameras.Count; c++) {
			this._jobs[c].Complete();
			this._job_src_arrays[c].Dispose();
			this._compressed_images_length[c].Dispose();
		}

		while (!this._xmlQueue.IsEmpty) {
			Thread.Sleep(50);
		}

		for(int t=0; t<this._numberOfXmlThread; t++) {
			this._xmlThreads[t].Abort();
		}

	}

	private IEnumerator _CaptureCoroutine() {

		while (true) {

			// Restart the full computation time chrono
			this._computation_full_time_chrono.Restart();

			// Render all camera
			this._RenderCameras();

			// Process current requests
			yield return this._ProcessRequests(); 

			// Wait for jobs to complete
			yield return this._WaitForJobsToComplete();

			if(upload_or_write) {
				// Upload images
				_UploadImagesToBackend();
			} else {
				// Write to disk
				yield return _WriteImagesToDisk();
			}

			// Computation frame rate
			if(this.logFps) {
				Debug.LogError(1000f / this._computation_full_time_chrono.ElapsedMilliseconds);
			}

			// Did the whole list of camera
			if (this._camera_index == 0) {
				this._frame_counter++;
			}

			// Skip 1 frame
			yield return null;
		}

	}

	private void _RenderCameras() {
		if (this.exportXmlRectangles) {
			this._vehiclesColliders = this._vehicles.GetComponentsInChildren<Collider>();
		}

		for (int i = 0; i < this._cameras.Count; i++) { 
			Camera c = this._cameras[i];

			// Render
			c.Render();

			// Enqueue async gpu request for the texture gpu -> cpu transfert
			this._requests.Enqueue(AsyncGPUReadback.Request(c.targetTexture, 0, TextureFormat.RGB24));


			// Find if a vehicle is in the camera view
			if (this.exportXmlRectangles) {

				this._vehiclesRendered.Clear();

				for (int coll = 0; coll < this._vehiclesColliders.Length; coll++) {

					Collider collider = this._vehiclesColliders[coll];

					// Test if the obj is in the field of view
					if (GeometryUtility.TestPlanesAABB(this._planes[i], collider.bounds)) {

						//Test if the obj is facing us
						if(Vector3.Angle(collider.transform.forward, c.transform.position - collider.transform.position) < 45) {
							this._vehiclesRendered.Add(this._vehiclesColliders[coll].gameObject);
						}

					}
				}

				this._SaveBoundingRectAsXml(i);
			}
		}
	}

	private IEnumerator _ProcessRequests() {
		// Each available request must be process in the same frame.
		// Async response have a life time of 1 frame and are ready at the start of it, else we wait on the next frame

		while (this._requests.Count > 0) {
			AsyncGPUReadbackRequest req = this._requests.Peek();

			if (req.done) {

				// Create a job (Encode and export image)
				this._ScheduleJob(req);

				// Clear request from the queue
				this._DequeueAsyncGpuReadBackRequest();

			} else if (req.hasError) {

				Debug.LogError("AsyncRequest Error");

				// Request is done
				this._DequeueAsyncGpuReadBackRequest();

			} else {
				// Give it 1 more frame
				yield return null;
				this._computation_allowed_time_chrono.Restart();
			}
		}

	}

	private void _DequeueAsyncGpuReadBackRequest() {
		// Remove request from queue
		this._requests.Dequeue();

		// Update camera index
		this._camera_index = (this._camera_index + 1) % this._cameras.Count;
	}

	private void _ScheduleJob(AsyncGPUReadbackRequest req) {
		// Get active job_arrays
		NativeArray<byte> job_src_array = this._job_src_arrays[this._camera_index];

		// Retrive data from the request
		job_src_array.CopyFrom(req.GetData<byte>());

		// Setup new job
		JpegEncodingJob job = new JpegEncodingJob {
			src = job_src_array,
			compressed_image_length = this._compressed_images_length[this._camera_index],
			frame_counter = this._frame_counter,
			camera_index = this._camera_index,
		};

		// Add job to the jobs queue
		this._jobs[this._camera_index] = job.Schedule();
	}

	private IEnumerator _WaitForJobsToComplete() {

		// Wait for jobs to complete
		for (int j = 0; j < this._jobs.Count; j++) {

			// If job is not completed, give it 1 more frame
			while (!this._jobs[j].IsCompleted) {
				yield return null;
				this._computation_allowed_time_chrono.Restart();
			}

			// Make sure we are done with it
			this._jobs[j].Complete();
		}

	}

	private void _UploadImagesToBackend() {

		WWWForm form = new WWWForm();

		for (int c = 0; c < this._cameras.Count; c++) {

			if (this._compressed_images_length[c][0] == 0) {
				Debug.LogError("Error in jpeg native plugin : " + c);
			} else {
				int compressed_image_length = this._compressed_images_length[c][0];
				string filename;

				if(this.exportXmlRectangles) {
					filename = c + "/car_" + this._frame_counter.ToString().PadLeft(3, '0');
				} else {
					filename = this._GetCameraName(c) + "_" + 0; // TODO add timestamps
				}

				form.AddBinaryData(
					"media",
					this._job_src_arrays[c].Slice(0, compressed_image_length).ToArray(), // TODO GC alloc every time
					 filename + ".jpg",
					"image/jpeg"
				);
			}

		}

		// Send the form
		UnityWebRequest handshake;
		if (this.exportXmlRectangles) {
			handshake = UnityWebRequest.Post("http://192.168.0.102:8080/upload/datasets", form);
		} else {
			handshake = UnityWebRequest.Post("http://192.168.0.102:8080/upload", form); // Jimme's home laptop
			//handshake = UnityWebRequest.Post("http://192.168.0.76:8080/upload", form); // Jimmy's home server
		}
		handshake.SendWebRequest();

		// We don't care about the response...
	}

	private IEnumerator _WriteImagesToDisk() {
		// FPS output drops drastically

		for (int c = 0; c < this._cameras.Count; c++) {

			if(this._computation_allowed_time_chrono.ElapsedMilliseconds > _computation_time) {
				yield return null;
				this._computation_allowed_time_chrono.Restart();
			}

			if (this._compressed_images_length[c][0] == 0) {
				Debug.LogError("Error in jpeg native plugin : " + c);
			} else {
				int compressed_image_length = this._compressed_images_length[c][0];

				File.WriteAllBytes(
					_dst_folder + this._GetCameraName(c) + "\\" + this._frame_counter + ".jpg",
					this._job_src_arrays[c].Slice(0, compressed_image_length).ToArray()
				);
			}

		}
	}

	private string _GetCameraName(Camera c) {
		//	return (c.transform.parent.parent.name[14] + "" + c.transform.name[8]).PadLeft(3);
		return c.transform.parent.parent.name + "_" + c.transform.name;
	}
	private string _GetCameraName(int c) {
		return this._GetCameraName(this._cameras[c]);
	}

	[DllImport("JpegEncodingWorkerDLL")]
	private static extern unsafe int _CompressImageInPlace(
		void *src,
		int width,
		int height,
		int jpeg_quality
	);

	private struct JpegEncodingJob : IJob {

		//[ReadOnly]
		public NativeArray<byte> src;

		[WriteOnly]
		public NativeArray<int> compressed_image_length;

		[ReadOnly]
		public int frame_counter;

		[ReadOnly]
		public int camera_index;

		public void Execute() {
			compressed_image_length[0] = 0;

			unsafe {
				// Use a native plugin to optimise jpeg encoding
				compressed_image_length[0] = _CompressImageInPlace(
					src.GetUnsafePtr(),
					_width,
					_height,
					_jpeg_quality
				);
			}
		}

	}

	private void _SaveBoundingRectAsXml(int camera_index) {
		string file_name = "car_" + this._frame_counter.ToString().PadLeft(3, '0');
		Camera camera = this._cameras[camera_index];
		
		XDocument doc = new XDocument(
			new XElement("annotation",
				new XElement("folder", camera_index.ToString()),
				new XElement("filename", file_name),
				new XElement("path", "./carTraining/Simulation/Datasets/" + file_name + ".jpg"),
				new XElement("source",
					new XElement("database", "Unknown")
				),
				new XElement("size",
					new XElement("width", _width),
					new XElement("height", _height),
					new XElement("depth", 3)
				),
				new XElement("segmented", 0)
			)
		);

		for(int i=0; i<this._vehiclesRendered.Count; i++) {
			Rect r = this._getObjBoundingRect(this._vehiclesRendered[i], camera);

			doc.Element("annotation").Add(
				new XElement("object",
					new XElement("name", "car"),
					new XElement("pose", "Unspecified"),
					new XElement("truncated", 0),
					new XElement("difficult", 0),
					new XElement("bndbox",
						new XElement("xmin", (int) r.xMin),
						new XElement("ymin", (int) r.yMin),
						new XElement("xmax", (int) r.xMax),
						new XElement("ymax", (int) r.yMax)
					)
				)
			);

		}

		this._xmlQueue.Enqueue(new XmlJobData {
			path = _dst_folder + "Datasets\\" + camera_index + "\\" + file_name + ".xml",
			doc = doc
		});
	}

	private struct XmlJobData {
		public string path;
		public XDocument doc;
	}

	private Rect _getObjBoundingRect(GameObject obj, Camera camera) {
		Transform obj_bounds = obj.transform.GetChild(0);
		Vector3[] verts = obj_bounds.GetComponent<MeshFilter>().mesh.vertices;

		for (int v = 0; v < verts.Length; v++) {
			verts[v] = obj_bounds.TransformPoint(verts[v]);
			//verts[v] = obj.transform.TransformPoint(verts[v]);
			verts[v] = camera.WorldToScreenPoint(verts[v]);
			verts[v].y = _height - verts[v].y;
		}

		Vector3 min = verts[0];
		Vector3 max = verts[0];

		for (int v = 0; v < verts.Length; v++) {
			min = Vector3.Min(min, verts[v]);
			max = Vector3.Max(max, verts[v]);
		}

		return Rect.MinMaxRect(min.x, min.y, max.x, max.y);
	}
}